import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Home from '../screens/home'
import Category from '../screens/catagories'
import SubCategory from '../screens/subCategories'
import CategoryEditor from '../screens/categoryEditor';
import SubCategoryEditor from '../screens/subCategoryEditor';

const Stack = createNativeStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="Category" component={Category} options={{ headerShown: false }} />
                <Stack.Screen name="CategoryEditor" component={CategoryEditor} options={{ headerShown: false }} />
                <Stack.Screen name="SubCategoryEditor" component={SubCategoryEditor} options={{ headerShown: false }} />
                <Stack.Screen name="SubCategory" component={SubCategory} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;