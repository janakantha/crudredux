/**
 * @format
 */
 import React from 'react';
import {AppRegistry} from 'react-native';
import { name as appName } from './app.json';

import { Provider } from 'react-redux';

import store from './src/redux/store';

import NvigatigationRouter from './src/navigation'

const app = () => (
    <Provider store={store}>
        <NvigatigationRouter />
    </Provider>
);

AppRegistry.registerComponent(appName, () => app);
