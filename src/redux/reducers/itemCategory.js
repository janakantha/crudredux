import * as type from "../types";

const initialState = {
    itemCategories: [],
    categoryObject: {},
    loading: false,
    message: '',
    error: null,
};

export default function itemCategories(state = initialState, action) {
    switch (action.type) {
        case type.GET_ITEMCATEGORY_REQUESTED:
            return {
                ...state,
                loading: true,
            };
        case type.GET_ITEMCATEGORY_UPDATE:
            return {
                ...state,
                loading: true,
            };
        case type.GET_ITEMCATEGORY_DELETE:
            return {
                ...state,
                loading: true,
            };
        case type.GET_ITEMCATEGORY_BY_ID:
            return {
                ...state,
                loading: true,
            };
        case type.GET_ITEMCATEGORY_ADD:
            return {
                ...state,
                loading: true,
            };
            case type.GET_ITEMCATEGORY_ADD_SUCCESS:
                return {
                    ...state,
                    loading: false,
                    message: action.message,
                };
            case type.GET_ITEMCATEGORY_ADD_FAILED:
                return {
                    ...state,
                    loading: false,
                    error: action.error,
                };
        case type.GET_ITEMCATEGORY_SUCCESS:
            return {
                ...state,
                loading: false,
                itemCategories: action.itemCategories,
            };
        case type.GET_ITEMCATEGORY_UPDATE_SUCCESS:
            return {
                ...state,
                loading: false,
                message: action.message,
            };
        case type.GET_ITEMCATEGORY_DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                message: action.message,
            };
        case type.GET_ITEMCATEGORY_BY_ID_SUCCESS:
            return {
                ...state,
                loading: false,
                categoryObject: action.categoryObject,
            };
        case type.GET_ITEMCATEGORY_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_ITEMCATEGORY_DELETE_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_ITEMCATEGORY_UPDATE_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_ITEMCATEGORY_BY_ID_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
}
