import { combineReducers } from "redux";

import ItemCategory from './itemCategory';
import SubCategory from './subCategory';

const rootReducer = combineReducers({

    itemCategory: ItemCategory,
    subCategory: SubCategory
});

export default rootReducer;