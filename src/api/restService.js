let BASE_URL = 'http://baseapi30.hit.lk';

export const setBaseURL = (baseURL) => {
    BASE_URL = baseURL;
};

export const EVENT_TYPES = {
    UNAUTHORIZED: 'unauthorized',
    UNKNOWN: 'unknown',
};

const validateBaseURL = () => {
    if (!BASE_URL) {
        throw new Error('Please set a Base URL using `setBaseUrl(baseURL=) => method');
    }
};

const defaultRequestHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};

const createURLParams = (params) => {
    let convertedParams = '';

    if (params) {
        Object
            .keys(params)
            .forEach((key, index) => {
                convertedParams += `${index === 0 ? '?' : '&'}${key}=${(params || {})[key]}`;
            });
    }

    return convertedParams;
};

const handleResponseStatus = async (response) => {

   

    if (response && response.status < 200 || response.status >= 300) {
        const error = new Error(response.statusText);
        error.response = await response.json();

        if (response.status === 401) {

            //eventHandler && eventHandler(EVENT_TYPES.UNAUTHORIZED, error);   
        }

        console.log('rez err  ', error)
        throw error;

    }

    let resp = await response.json()

    console.log('rez  ', JSON.stringify(resp))

    return resp;
   // return response.json();
};

export const GET = async (endpoint, headers, queryParams) => {

    validateBaseURL();

    const url = `${BASE_URL}/${endpoint}${createURLParams(queryParams)}`;
    const options = {
        headers: {
            ...defaultRequestHeaders,
            ...headers,
        },
    };
    let response = {};

    try {
        response = await fetch(url, options);
    } catch (error) {
        console.log('in get ', error);
    }

    return handleResponseStatus(response);
};

export const POST =
    async (endpoint, headers, queryParams, bodyParams) => {

        validateBaseURL();

        const url = `${BASE_URL}/${endpoint}${createURLParams(queryParams)}`;

        const options = {
            method: 'POST',
            headers: {
                ...defaultRequestHeaders,
                ...headers,
            },

            body: JSON.stringify(bodyParams || {}),
        };
       // const response = await fetch(url, options);

        let response = {};

        try {
            response = await fetch(url, options);
        } catch (error) {
            console.log('in Post ', error);
        }

        return handleResponseStatus(response);
    };

    export const PUT =
    async (endpoint, headers, queryParams, bodyParams) => {

        validateBaseURL();

        const url = `${BASE_URL}/${endpoint}${createURLParams(queryParams)}`;

        const options = {
            method: 'PUT',
            headers: {
                ...defaultRequestHeaders,
                ...headers,
            },

            body: JSON.stringify(bodyParams || {}),
        };
       // const response = await fetch(url, options);

        let response = {};

        try {
            response = await fetch(url, options);
        } catch (error) {
            console.log('in PUT ', error);
        }

        return handleResponseStatus(response);
    };

    export const DELETE =
    async (endpoint, headers, queryParams, bodyParams) => {

        validateBaseURL();

        const url = `${BASE_URL}/${endpoint}${createURLParams(queryParams)}`;

        const options = {
            method: 'DELETE',
            headers: {
                ...defaultRequestHeaders,
                ...headers,
            },

            body: JSON.stringify(bodyParams || {}),
        };
       // const response = await fetch(url, options);

        let response = {};

        try {
            response = await fetch(url, options);
        } catch (error) {
            console.log('in DELETE ', error);
        }

        return handleResponseStatus(response);
    };