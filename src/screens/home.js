import React, { useEffect } from 'react'
import { View, Text, StatusBar, Image, FlatList, TouchableOpacity } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { useSelector, useDispatch } from "react-redux";
import Moment from 'moment'
import { getOperations } from '../redux/actions/operations';

const home = ({ navigation }) => {

    const dispatch = useDispatch();
    Moment.locale('en');

    const operations = []
    // const operations = useSelector((state) => state.operations.operations);
    // const loading = useSelector((state) => state.operations.loading);
    // const error = useSelector((state) => state.operations.error);

    useEffect(() => {
        //  dispatch(getOperations());

    }, []);

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
            <View style={{ width: '94%', marginTop: 20, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: '3%' }}>
                <View>
                    <Text style={{ fontSize: 14, fontFamily: 'SF-Compact-Display-Thin', color: '#1B3C68' }}>Welcome back,</Text>
                    <Text style={{ fontSize: 22, fontFamily: 'SF-Compact-Display-Semibold', color: '#1B3C68', lineHeight: 29 }}>Julio Oliver</Text>
                </View>
                <Image style={{ height: 50, width: 50 }} source={require('../assets/user.png')} />
            </View>
            <View style={{ marginTop: 15 }}>
                <Image resizeMode={'stretch'} style={{ width: '100%', height: 230 }} source={require('../assets/cards.png')} />
            </View>
            <View style={{ marginTop: 5, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 9, height: 9, backgroundColor: '#C4C4C4', borderRadius: 10, marginRight: 7 }} />
                <View style={{ width: 10, height: 10, backgroundColor: '#0074E4', borderRadius: 10, marginRight: 7 }} />
                <View style={{ width: 9, height: 9, backgroundColor: '#C4C4C4', borderRadius: 10 }} />
            </View>

            <View style={{ width: '94%', marginTop: 20, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: '3%', }}>
                <TouchableOpacity onPress={() => navigation.navigate('Category')}
                    style={{
                        width: 110, height: 85, backgroundColor: '#0074E4', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', borderRadius: 10, paddingVertical: 10,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,
                    }}>
                    <Image resizeMode={'contain'} style={{ width: 40, height: 40 }} source={require('../assets/doller.png')} />
                    <Text style={{ fontSize: 16, fontFamily: 'SF-Pro-Display-Medium', color: '#fff', lineHeight: 18 }}>Categories</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Category')}
                    style={{
                        width: 110, height: 85, backgroundColor: '#fff', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', borderRadius: 10, paddingVertical: 10,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.62,

                        elevation: 4,
                    }}>
                    <Image resizeMode={'contain'} style={{ width: 40, height: 40 }} source={require('../assets/stat.png')} />
                    <Text style={{ fontSize: 14, fontFamily: 'SF-Pro-Display-Medium', color: '#0074E4', lineHeight: 16 }}>Sub-Categories</Text>
                </TouchableOpacity>
                <View style={{
                    width: 110, height: 85, backgroundColor: '#fff', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', borderRadius: 10, paddingVertical: 10,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,

                    elevation: 4,
                }}>
                    <Image resizeMode={'contain'} style={{ width: 40, height: 40 }} source={require('../assets/uplus.png')} />
                    <Text style={{ fontSize: 16, fontFamily: 'SF-Pro-Display-Medium', color: '#0074E4', lineHeight: 18 }}>Settings</Text>
                </View>
            </View>
            <View style={{ position:'absolute',left:0,bottom:0, width: '100%', marginTop: 20, height: 80, backgroundColor: '#fff', paddingHorizontal: 40, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image resizeMode={'contain'} style={{ width: 20, height: 20, marginBottom: 5 }} source={require('../assets/pur.png')} />
                    <Text style={{ fontSize: 14, fontFamily: 'SF-Pro-Text-Medium', color: '#0074E4', lineHeight: 18 }}>Wallet</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Currency')} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image resizeMode={'contain'} style={{ width: 20, height: 20, marginBottom: 5 }} source={require('../assets/ex.png')} />
                    <Text style={{ fontSize: 14, fontFamily: 'SF-Pro-Text-Medium', color: '#A8A8A8', lineHeight: 18 }}>Wallet</Text>
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image resizeMode={'contain'} style={{ width: 20, height: 20, marginBottom: 5 }} source={require('../assets/pro.png')} />
                    <Text style={{ fontSize: 14, fontFamily: 'SF-Pro-Text-Medium', color: '#A8A8A8', lineHeight: 18 }}>Wallet</Text>
                </View>
            </View>
        </View>
    )
}

export default home
