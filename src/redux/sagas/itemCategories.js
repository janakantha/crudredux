import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";

import * as type from "../types";

import ItemCategoryServices from '../../services/itemCategoryServices';


function* fetchItemCategory(action) {

    if (action.keyword) {
        try {
            const itemCategories = yield call(ItemCategoryServices.fetchItemCategoryList, action.keyword);
            console.log('fetchItemCategory act ===>', action)
            yield put({ type: type.GET_ITEMCATEGORY_SUCCESS, itemCategories: itemCategories });
        } catch (e) {
            console.log('fetchItemCategory err ===>', e)
            yield put({ type: type.GET_ITEMCATEGORY_FAILED, error: e.message });
        }

    } else {
        try {
            const itemCategories = yield call(ItemCategoryServices.fetchItemCategoryList);
            console.log('fetchItemCategory act ===>', action)
            yield put({ type: type.GET_ITEMCATEGORY_SUCCESS, itemCategories: itemCategories });
        } catch (e) {
            console.log('fetchItemCategory err ===>', e)
            yield put({ type: type.GET_ITEMCATEGORY_FAILED, error: e.message });
        }

    }
}

function* fetchItemCategoryById(action) {
    try {
        const categoryObject = yield call(ItemCategoryServices.fetchItemCategoryById, action.id);
        console.log('fetchItemCategoryById act ===>', action)
        yield put({ type: type.GET_ITEMCATEGORY_BY_ID_SUCCESS, categoryObject: categoryObject });
    } catch (e) {
        console.log('fetchItemCategoryById err ===>', e)
        yield put({ type: type.GET_ITEMCATEGORY_BY_ID_FAILED, error: e.message });
    }
}

function* addItemCategory(action) {

    try {
        const resp = yield call(ItemCategoryServices.addItemCategoryList, action.category);

        console.log('addItemCategory resp ', JSON.stringify(resp))

        yield put({ type: type.GET_ITEMCATEGORY_ADD_SUCCESS, message: resp.Message });
    } catch (e) {
        yield put({ type: type.GET_ITEMCATEGORY_ADD_FAILED, error: e.message });
    }
}

function* updateItemCategory(action) {

    try {
        const resp = yield call(ItemCategoryServices.updateItemCategory, action.categoryObject);

        yield put({ type: type.GET_ITEMCATEGORY_UPDATE_SUCCESS, message: resp.Message });

    } catch (e) {
        yield put({ type: type.GET_ITEMCATEGORY_UPDATE_FAILED, error: e.message });
    }
}

function* deleteItemCategory(action) {

    try {
        yield call(ItemCategoryServices.deleteItemCategory, action.categoryObject);
        yield put({ type: type.GET_ITEMCATEGORY_DELETE_SUCCESS, });
    } catch (e) {
        yield put({ type: type.GET_ITEMCATEGORY_DELETE_FAILED, error: e.message });
    }
}

function* itemCategoriesSaga() {
    yield takeLatest(type.GET_ITEMCATEGORY_REQUESTED, fetchItemCategory);
}

function* itemCategoryByIdSaga() {
    yield takeLatest(type.GET_ITEMCATEGORY_BY_ID, fetchItemCategoryById);
}

function* addItemCategoriesSaga() {
    yield takeLatest(type.GET_ITEMCATEGORY_ADD, addItemCategory);
}

function* itemCategoryUpdateSaga() {
    yield takeLatest(type.GET_ITEMCATEGORY_UPDATE, updateItemCategory);
}

function* itemCategoryDeleteSaga() {
    yield takeLatest(type.GET_ITEMCATEGORY_DELETE, deleteItemCategory);
}



export default function* rootSaga() {
    yield all([
        itemCategoriesSaga(),
        addItemCategoriesSaga(),
        itemCategoryByIdSaga(),
        itemCategoryUpdateSaga(),
        itemCategoryDeleteSaga()
    ])
}
