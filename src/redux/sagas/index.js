import { all } from 'redux-saga/effects'

import ItemCategorySaga from './itemCategories'

import SubCategorySaga from './subCategories'

export default function* rootSaga() {
  yield all([
   ItemCategorySaga(),
   SubCategorySaga()
  ])
}