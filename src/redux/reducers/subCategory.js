import * as type from "../types";

const initialState = {
    subCategories: [],
    subcategoryObject: {},
    loading: false,
    message:'',
    error: null,
};

export default function subCategories(state = initialState, action) {
    switch (action.type) {
        case type.GET_SUBCATEGORY_REQUESTED:
            return {
                ...state,
                loading: true,
            };
        case type.GET_SUBCATEGORY_SUCCESS:
            return {
                ...state,
                loading: false,
                subCategories: action.subCategories,
            };

        case type.GET_SUBCATEGORY_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_SUBCATEGORY_ADD:
            return {
                ...state,
                loading: true,
            };
        case type.GET_SUBCATEGORY_ADD_SUCCESS:
            return {
                ...state,
                message:action.message,
                loading: false,
            };
        case type.GET_SUBCATEGORY_ADD_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_SUBCATEGORY_BY_ID:
            return {
                ...state,
                loading: true,
            };
        case type.GET_SUBCATEGORY_BY_ID_SUCCESS:
            return {
                ...state,
                loading: false,
                subcategoryObject: action.subcategoryObject,
            };
        case type.GET_SUBCATEGORY_BY_ID_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_SUBCATEGORY_UPDATE:
            return {
                ...state,
                loading: true,
            };
        case type.GET_SUBCATEGORY_UPDATE_SUCCESS:
            return {
                ...state,
                message:action.message,
                loading: false,
            };
        case type.GET_SUBCATEGORY_UPDATE_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        case type.GET_SUBCATEGORY_DELETE:
            return {
                ...state,
                loading: true,
            };
        case type.GET_SUBCATEGORY_DELETE_SUCCESS:
            return {
                ...state,
                message:action.message,
                loading: false,
            };
        case type.GET_SUBCATEGORY_DELETE_FAILED:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        default:
            return state;
    }
}
