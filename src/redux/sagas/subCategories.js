import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";

import * as type from "../types";

import SubCategoryService from "../../services/subCategoryServices";

function* fetchSubCategory(action) {

    if (action.detail.keyword) {
        try {
            const subCategories = yield call(SubCategoryService.fetchSubCategoryList, action);
            console.log('fetchSubCategory act ===>', action)
            yield put({ type: type.GET_SUBCATEGORY_SUCCESS, subCategories: subCategories });
        } catch (e) {
            console.log('fetchSubCategory err ===>', e)
            yield put({ type: type.GET_SUBCATEGORY_FAILED, error: e.message });
        }

    } else {
        try {
            const subCategories = yield call(SubCategoryService.fetchSubCategoryList, action);
            console.log('fetchSubCategory data  ===>', subCategories)
            yield put({ type: type.GET_SUBCATEGORY_SUCCESS, subCategories: subCategories });
        } catch (e) {
            console.log('fetchSubCategory err ===>', e)
            yield put({ type: type.GET_SUBCATEGORY_FAILED, error: e.message });
        }
    }
}

function* addSubCategory(action) {

    try {
        const resp = yield call(SubCategoryService.addSubCategoryList, action.subCategory);
        yield put({ type: type.GET_SUBCATEGORY_ADD_SUCCESS, message: resp.Message });
    } catch (e) {
        yield put({ type: type.GET_SUBCATEGORY_ADD_FAILED, error: e.message });
    }
}

function* fetchSubCategoryById(action) {
    try {
        const subcategoryObject = yield call(SubCategoryService.fetchSubCategoryById, action.id);
        console.log('fetchSubCategoryById act ===>', action)
        console.log('fetchSubCategoryById data ===>', subcategoryObject)
        yield put({ type: type.GET_SUBCATEGORY_BY_ID_SUCCESS, subcategoryObject: subcategoryObject });
    } catch (e) {
        console.log('fetchSubCategoryById err ===>', e)
        yield put({ type: type.GET_SUBCATEGORY_BY_ID_FAILED, error: e.message });
    }
}

function* updateSubCategory(action) {

    try {
        const resp = yield call(SubCategoryService.updateSubCategory, action.categoryObject);
        yield put({ type: type.GET_SUBCATEGORY_UPDATE_SUCCESS, message: resp.Message });
    } catch (e) {
        yield put({ type: type.GET_SUBCATEGORY_UPDATE_FAILED, error: e.message });
    }
}

function* deleteSubCategory(action) {

    try {
        yield call(SubCategoryService.deleteSubCategory, action.categoryObject);
        yield put({ type: type.GET_SUBCATEGORY_DELETE_SUCCESS, });
    } catch (e) {
        yield put({ type: type.GET_SUBCATEGORY_DELETE_FAILED, error: e.message });
    }
}


function* subCategoriesSaga() {
    yield takeLatest(type.GET_SUBCATEGORY_REQUESTED, fetchSubCategory);
}

function* addSubCategorySaga() {
    yield takeLatest(type.GET_SUBCATEGORY_ADD, addSubCategory);
}

function* subCategoryByIdSaga() {
    yield takeLatest(type.GET_SUBCATEGORY_BY_ID, fetchSubCategoryById);
}

function* subCategoryUpdateSaga() {
    yield takeLatest(type.GET_SUBCATEGORY_UPDATE, updateSubCategory);
}

function* subCategoryDeleteSaga() {
    yield takeLatest(type.GET_SUBCATEGORY_DELETE, deleteSubCategory);
}

export default function* rootSaga() {
    yield all([
        subCategoriesSaga(),
        addSubCategorySaga(),
        subCategoryByIdSaga(),
        subCategoryUpdateSaga(),
        subCategoryDeleteSaga()
    ])
}