import * as type from "../types";

export function getitemCategory(keyword) {
  return {
    type: type.GET_ITEMCATEGORY_REQUESTED,
    keyword
  };
}

export function getitemCategoryById(id) {
  return {
    type: type.GET_ITEMCATEGORY_BY_ID,
    id
  };
}

export function updateitemCategory(categoryObject) {
  return {
    type: type.GET_ITEMCATEGORY_UPDATE,
    categoryObject
  };
}

export function deleteitemCategory(categoryObject) {
  return {
    type: type.GET_ITEMCATEGORY_DELETE,
    categoryObject
  };
}

export function additemCategory(category) {
  return {
    type: type.GET_ITEMCATEGORY_ADD,
    category
  };
}
