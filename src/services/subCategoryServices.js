import { DELETE, GET, POST, PUT } from '../api/restService';

const urlFactory = {

    fetchSubCategories: () => `api/ItemSubCategory/AdvancedSearch`,
    addSubCategory: () => `api/ItemSubCategory`,
    fetchSubCategory: (id) => `api/ItemSubCategory/${id}`,
    updateSubCategory: () => `api/ItemSubCategory`,
    deleteSubCategory: (id) => `api/ItemSubCategory/${id}`,
};

const SubCategoryService = {

    fetchSubCategoryList: async (action) => {

        console.log('fetchSubCategoryList ', action)

        const endpoint = urlFactory.fetchSubCategories();

        let queryParams = {
            searchText: '',
            pageSize: 15,
            pageNum: 1,
            category: action.detail.ID
        };

        if (action.detail.keyword) {
            queryParams = {
                searchText: action.detail.keyword,
                pageSize: 15,
                pageNum: 1,
                category: action.detail.ID
            };
        }

        const headers = {};

        return await GET(endpoint, headers, queryParams);
    },
    addSubCategoryList: async (action) => {
        console.log('addSubCategoryList ', action)
        const endpoint = urlFactory.addSubCategory();

        const headers = {};

        return await POST(endpoint, headers, '', action);
    },
    fetchSubCategoryById: async (id) => {

        const endpoint = urlFactory.fetchSubCategory(id);

        let queryParams = {};

        const headers = {};

        return await GET(endpoint, headers, queryParams);
    },
    updateSubCategory: async (categoryObject) => {

        console.log('updateSubCategory Object ', JSON.stringify(categoryObject))

        const endpoint = urlFactory.updateSubCategory();

        const headers = {};

        return await PUT(endpoint, headers, '', categoryObject);
    },
    deleteSubCategory: async (categoryObject) => {

        console.log('deleteSubCategory categoryObject ', JSON.stringify(categoryObject))

        let catObj = {
            Name: categoryObject.Name,
            Description: "",
            Order: 1,
            Active: "true"

        }

        const endpoint = urlFactory.deleteSubCategory(categoryObject.ID);

        const headers = {};

        return await DELETE(endpoint, headers, '', catObj);
    }

}

export default SubCategoryService;