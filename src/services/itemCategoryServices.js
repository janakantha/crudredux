import { DELETE, GET, POST, PUT } from '../api/restService';

const urlFactory = {

    fetchItemCategories: () => `api/ItemCategory/AdvancedSearch`,
    addItemCategory: () => `api/ItemCategory`,
    fetchItemCategory: (id) => `api/ItemCategory/${id}`,
    updateItemCategory: () => `api/ItemCategory`,
    deleteItemCategory: (id) => `api/ItemCategory/${id}`,
};

const ItemCategoryService = {
    fetchItemCategoryList: async (keyword) => {

        //   console.log('fetchItemCategoryList ', keyword)

        const endpoint = urlFactory.fetchItemCategories();

        let queryParams = {
            searchText: '',
            pageSize: 99,
            pageNum: 1
        };

        if (keyword) {
            queryParams = {
                searchText: keyword,
                pageSize: 15,
                pageNum: 1
            };
        }

        const headers = {};

        return await GET(endpoint, headers, queryParams);

    },
    addItemCategoryList: async (category) => {

        console.log('addItemCategory name ', category)

        let newCategory = {
            Name: category,
            Description: "",
            Order: 1,
            Active: true
        }

        const endpoint = urlFactory.addItemCategory();

        const headers = {};

        return await POST(endpoint, headers, '', newCategory);
    },
    fetchItemCategoryById: async (id) => {

        const endpoint = urlFactory.fetchItemCategory(id);

        let queryParams = {};

        const headers = {};

        return await GET(endpoint, headers, queryParams);
    },
    updateItemCategory: async (categoryObject) => {

        console.log('updateItemCategory categoryObject ', JSON.stringify(categoryObject))

        const endpoint = urlFactory.updateItemCategory();

        const headers = {};

        return await PUT(endpoint, headers, '', categoryObject);
    },
    deleteItemCategory: async (categoryObject) => {

        console.log('deleteItemCategory categoryObject ', JSON.stringify(categoryObject))

        let catObj = {
            Name: categoryObject.Name,
            Description: "",
            Order: 1,
            Active: "true"

        }

        const endpoint = urlFactory.deleteItemCategory(categoryObject.ID);

        const headers = {};

        return await DELETE(endpoint, headers, '', catObj);
    }

}

export default ItemCategoryService;