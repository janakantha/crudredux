import React, { useState, useEffect } from 'react'
import { View, Text, StatusBar, TouchableHighlight, Image, TextInput, TouchableOpacity, RefreshControl } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { useSelector, useDispatch } from "react-redux";
import { ActivityIndicator, Colors } from 'react-native-paper';
import { SwipeListView } from 'react-native-swipe-list-view';

import { getSubCategory, deleteSubCategory } from '../redux/actions/subCategory';
import { getitemCategoryById } from '../redux/actions/itemCategory';


const subCategories = ({ route, navigation }) => {

    const dispatch = useDispatch();

    const [itemId, setItemId] = useState(route.params.categoryId)

    const itemCatObj = useSelector((state) => state.itemCategory.categoryObject);
    const itemloading = useSelector((state) => state.itemCategory.loading);
    const itemerror = useSelector((state) => state.itemCategory.error);

    const subCategories = useSelector((state) => state.subCategory.subCategories);
    const subloading = useSelector((state) => state.subCategory.loading);
    const suberror = useSelector((state) => state.subCategory.error);

    useEffect(() => {

        console.log('props ', itemId)

        if (itemId) {

            dispatch(getitemCategoryById(itemId))

            console.log('categoryObject ', itemCatObj)

            dispatch(getSubCategory({ ID: itemId }))
        }

        //console.log('categoryObject ', JSON.stringify())

    }, []);

    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        wait(2000).then(() => setRefreshing(false));
    }, []);

    const wait = (timeout) => {
        if (itemId) {

            dispatch(getitemCategoryById(itemId))

            console.log('categoryObject ', itemCatObj)

            dispatch(getSubCategory({ ID: itemId }))
        }
        return new Promise(resolve => setTimeout(resolve, timeout));
    }



    const getSubCategories = (keyword) => {

        dispatch(getSubCategory({ keyword: keyword, ID: itemId }))

    };

    const deleteCategory = (item) => {

        console.log(item)

        dispatch(deleteSubCategory(item))

    };

    const onRowDidOpen = rowKey => {

    };


    const renderItem = data => {
        return (
            <TouchableHighlight >
                <View style={{ backgroundColor: '#fff', height: 60, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 10, }}>
                    <View style={{ width: 35, height: 35, marginRight: 10, backgroundColor: '#F7F8FB', flexDirection: 'row', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontFamily: 'SF-Pro-Text-SemiBold', color: '#1B3C68', lineHeight: 19, }}>A</Text>
                    </View>
                    <Text style={{ fontSize: 16, fontFamily: 'SF-Pro-Text-Medium', color: '#1B3C68', lineHeight: 19, }}>{data.item.Name}</Text>
                </View>
            </TouchableHighlight>
        )

    }

    const itemSeparator = data => (
        <View style={{ width: wp('90%'), flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}>
            <View style={{ width: wp('86%'), height: 1, backgroundColor: 'rgba(113,133,160,0.5)' }} />
        </View>
    )

    const renderHiddenItem = (data, rowMap) => (
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 15 }} >
            <TouchableOpacity onPress={() => navigation.navigate('SubCategoryEditor', {
                itemId: data.item.ID,
            })}>
                <Image style={{ resizeMode: 'contain', height: 30, width: 20, tintColor: 'blue' }}
                    source={require('../assets/pen.png')} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => deleteCategory(data.item)}>
                <Image style={{ resizeMode: 'contain', height: 30, width: 20, tintColor: 'red' }}
                    source={require('../assets/delete.png')} />
            </TouchableOpacity>
        </View>
    );


    return (

        <View style={{ flex: 1, backgroundColor: '#0074E4' }}>
            <StatusBar backgroundColor={'#0074E4'} barStyle="light-content" />
            <View style={{ paddingHorizontal: wp('5%'), marginTop: 10 }}>
                <View style={{ width: wp('90%'), height: 50, borderRadius: 10, backgroundColor: '#1B3C68' }}></View>
            </View>
            <View style={{
                position: 'absolute', top: hp('2.5%'), left: 0, width: wp('100%'), height: hp('95%'), backgroundColor: '#F7F8FB', borderRadius: 15,
            }}>
                <View style={{ width: wp('100%'), paddingHorizontal: 5, marginTop: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => navigation.goBack()} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Image resizeMode={'contain'} style={{ height: 26, width: 16 }} source={require('../assets/back.png')} />
                        <Text style={{ fontSize: 18, fontFamily: 'SF-Pro-Text-Regular', color: '#007AFF', lineHeight: 19, marginLeft: 5 }}>Home</Text>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 15, fontFamily: 'SF-Pro-Text-Semibold', color: '#1B3C68', lineHeight: 17, }}>
                        {itemCatObj.DataObject ? `Category: ${itemCatObj.DataObject.Name}` : `Loading...`}
                    </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('SubCategoryEditor', {
                        itemId: undefined,
                    })} style={{ width: 40 }} >
                        <Image resizeMode={'contain'} style={{ height: 30, width: 30, tintColor: '#007AFF' }} source={require('../assets/plus2.png')} />
                    </TouchableOpacity>
                </View>
                <View style={{ width: wp('100%'), paddingHorizontal: wp('5%'), marginTop: 10 }}>
                    <View style={{
                        width: wp('90%'), borderRadius: 20, height: 45, marginTop: 10, flexDirection: 'row', backgroundColor: '#fff', justifyContent: 'flex-start', alignItems: 'center',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,

                        elevation: 6,
                    }}>
                        <Image resizeMode={'contain'} style={{ height: 18, width: 16, marginLeft: 10 }} source={require('../assets/search.png')} />

                        <TextInput placeholderTextColor={'grey'} placeholder='Search SubCategory'
                            style={{ fontSize: 15, fontFamily: 'SF-Pro-Text-Regular', color: '#000', lineHeight: 20, marginLeft: 10 }}
                            onChangeText={(text) => getSubCategories(text)} />
                    </View>
                    <View style={{
                        width: wp('89%'), height: hp('75%'), padding: 10, borderRadius: 20, marginTop: 20, backgroundColor: '#fff', margin: wp('0.5%'),
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,
                        elevation: 6,
                    }}>
                        {
                            subCategories.ListOfObjects ?
                                <SwipeListView
                                    data={subCategories.ListOfObjects}
                                    renderItem={renderItem}
                                    renderHiddenItem={renderHiddenItem}
                                    leftOpenValue={45}
                                    rightOpenValue={-45}
                                    previewRowKey={'0'}
                                    previewOpenValue={-40}
                                    previewOpenDelay={1000}
                                    onRowDidOpen={onRowDidOpen}
                                    ItemSeparatorComponent={itemSeparator}
                                    showsVerticalScrollIndicator={false}
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={refreshing}
                                            onRefresh={onRefresh}
                                        />
                                    }

                                />
                                :
                                <View style={{ height: 350, alignItems: 'center', justifyContent: 'center' }}>
                                    <ActivityIndicator style={{ alignSelf: 'center' }} size={'large'} animating={true} color={Colors.blue200} />
                                </View>}
                    </View>
                </View>
            </View>
        </View>
    );
};

export default subCategories;
