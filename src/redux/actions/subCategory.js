import * as type from "../types";

export function getSubCategory(detail) {
  return {
    type: type.GET_SUBCATEGORY_REQUESTED,
    detail
  };
}

export function addSubCategory(subCategory) {
  return {
    type: type.GET_SUBCATEGORY_ADD,
    subCategory
  };
}

export function getSubCategoryById(id) {
  return {
    type: type.GET_SUBCATEGORY_BY_ID,
    id
  };
}

export function updateSubCategory(categoryObject) {
  return {
    type: type.GET_SUBCATEGORY_UPDATE,
    categoryObject
  };
}

export function deleteSubCategory(categoryObject) {
  return {
    type: type.GET_SUBCATEGORY_DELETE,
    categoryObject
  };
}