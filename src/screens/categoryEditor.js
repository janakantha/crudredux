import React, { useState, useEffect } from 'react'
import { View, Text, StatusBar, ImageBackground, Image, TextInput, TouchableOpacity, Alert } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { useSelector, useDispatch } from "react-redux";

import { additemCategory, getitemCategoryById, updateitemCategory } from '../redux/actions/itemCategory';

const convertor = ({ route, navigation }) => {

    const dispatch = useDispatch();


    const [catName, setCatName] = useState('')

    const [itemId, setItemId] = useState(route.params.itemId)

    const [refreshing, setRefreshing] = useState(false);

    const [msg, setMsg] = useState(null);

    const itemCatObj = useSelector((state) => state.itemCategory.categoryObject);
    // //ListOfObjects
    const loading = useSelector((state) => state.itemCategory.loading);
    const Message = useSelector((state) => state.itemCategory.message);
    const error = useSelector((state) => state.itemCategory.error);

    useEffect(() => {

        console.log('props ', itemId)

        if (itemId) {
            dispatch(getitemCategoryById(itemId))
        }
        if (!loading) {
            console.log('categoryObject ', itemCatObj)
        }

    }, []);

    useEffect(() => {
        console.log('New value', Message) 
        setMsg(Message)
        return () => {
           console.log('Prev value', Message) 
          
        }
   
      }, [Message])

    const checkDispatch = () => {

        setRefreshing(true)

        if (itemId && itemCatObj.DataObject) {

            let item = itemCatObj.DataObject;

            let catObj = {
                ID: item.ID,
                Code: item.Code,
                Name: catName,
                Description: "updated by App",
                Order: 1,
                Active: true
            }

            console.log('update obj ', JSON.stringify(catObj))

            dispatch(updateitemCategory(catObj))

        } else {
            dispatch(additemCategory(catName))
        }

      createRespAlert();
    }

    const createRespAlert = () => {
       
            setRefreshing(false)

            Alert.alert(
                msg,
                "Success",
                [
                    { text: "Go Back", onPress: () => navigation.navigate('Category') }
                ]
            );
        


    }





    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar backgroundColor={'#0074E4'} barStyle="light-content" />
            <ImageBackground source={require('../assets/head.png')} imageStyle={{ resizeMode: 'stretch' }} style={{ width: wp('100%'), height: hp('40%') }}>
                <View style={{ marginTop: 45, marginLeft: 20 }}>
                    <Text style={{ fontSize: 24, fontFamily: 'SF-Compact-Display-Medium', color: '#fff', lineHeight: 25 }}>
                        {
                            itemId ? ' Update Category' : ' New Category'
                        }
                    </Text>
                </View>
            </ImageBackground>
            <View style={{
                position: 'absolute', top: hp('20%'), left: wp('10%'), width: wp('80%'), height: hp('22%'), paddingHorizontal: 15, backgroundColor: '#fff', borderRadius: 15, justifyContent: 'center',
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,

                elevation: 4,
            }}>
                <View style={{ marginTop: 25, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={{ fontSize: 16, fontFamily: 'SF-Pro-Text-Semibold', color: '#1B3C68', lineHeight: 19, marginRight: 4 }}>Name</Text>
                        <Image style={{ height: 13, width: 10 }} source={require('../assets/next.png')} />
                    </View>
                    <View style={{ width: 210, height: 50, backgroundColor: '#FAF7FD', borderRadius: 10, paddingLeft: 10, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', borderColor: '#0074E4', borderWidth: 1 }}>

                        <TextInput  placeholderTextColor={'grey'} placeholder={
                            itemCatObj.DataObject ? itemCatObj.DataObject.Name : 'Enter Category Name'}
                            style={{ fontSize: 15, fontFamily: 'SF-Pro-Text-Regular', color: '#000', lineHeight: 20, marginLeft: 10 }}
                            onChangeText={(text) => setCatName(text)}
                        />
                    </View>
                </View>
                {
                    refreshing ? <ActivityIndicator style={{ alignSelf: 'center' }} size={'large'} animating={true} color={Colors.blue200} />
                        :
                        <TouchableOpacity onPress={() => checkDispatch()} style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <Image style={{ height: 25, width: 25, marginRight: 10, tintColor: '#0074E4' }} source={
                                itemId ? require('../assets/pen.png') : require('../assets/plus.png')
                            } />
                            <Text style={{ fontSize: 17, fontFamily: 'SF-Pro-Text-Semibold', color: '#0074E4', lineHeight: 19, marginRight: 4 }}>
                                {
                                    itemId ? ' Update Category' : ' Add Category'
                                }
                            </Text>
                        </TouchableOpacity>
                }

            </View>
        </View>
    )
}

export default convertor
